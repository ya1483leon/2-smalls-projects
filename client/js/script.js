const counters = document.querySelectorAll('[data-counter]');

if(counters) {
    counters.forEach(counter => {
        counter.addEventListener('click', e => {
            const target = e.target;

                if (document.querySelector('input').value == '' && (target.classList.contains('counter__btn-minus') || target.classList.contains('counter__btn-plus'))) {
                    document.querySelector('input').value = 0;
                }
                let value = parseInt(document.querySelector('input').value);

                if(target.classList.contains('counter__btn-plus')) {
                    value++;
                } else {
                    --value;
                }

                if(value <= 0) {
                    value = '';
                    document.querySelector('.counter__btn-minus').classList.add('disabled');
                } else {
                    document.querySelector('.counter__btn-minus').classList.remove('disabled');
                }

                if(value > 10) {
                    value = '10';
                    document.querySelector('.counter__btn-plus').classList.add('disabled');
                } else {
                    document.querySelector('.counter__btn-plus').classList.remove('disabled');
                }
                
                document.querySelector('input').value = value;
        })
    })
}